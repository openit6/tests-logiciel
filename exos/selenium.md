# Work To Do

* test on the page of your choice
* Check the title of the pages with the ide
* Check for result after search
* You might need to add some pause before checking the page title (pause command with time to wait expressed in ms in target field)  
* Generate python and run from python

## Explication

Nous avons choisis la page [python.org](https://www.python.org/)

Voici le déroulé du test:

```txt
Ouvrir le navigateur
Aller sur la page https://www.python.org/
S'assurer que la page contient le mot Python

Dans la barre de recherche du site web rentrer Code of Conduct
CLicker sur le bouton de validation
S'assurer que la page contient le texte No results found.
```

## Les resultats

Voici les résultats : voir [results.log](selenium/results.log)

Avec le script suivant : voir [script](selenium/test_replicaterfw.py)

## Robotframework vs Selenium (sans surcouche)

Comme nous pouvons le constater, il y a une similitude entre Robot Framework et Selenium en ce qui concerne le résultat final. Néanmoins, il est important de noter que bien que les résultats soient identiques, les différences résident dans le script généré. Selenium produit du code Python pur de manière automatique, ce qui le rend lisible principalement par les développeurs. En revanche, Robot Framework est une surcouche de plus haut niveau, ce qui le rend plus verbeux et lisible par un public plus large. De plus, il permet d'obtenir le même résultat en utilisant moins de lignes de code.
