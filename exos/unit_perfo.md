# Unit performance tests using timeit

For the purpose of this demo we will compare several sort algorithms using timeit

* Define some test cases
  * random array
  * array is already sorted (for instance 1, 3, 5, 9, 11, ...)
  * array is already sorted but the min value is at the end (for instance 3, 5, 9, 11, ..., 1)
  * array is sorted but in reversed order (for instance 11, 9, 5, 3, ..., 1)
  * array is sorted but in reversed order and the max value is at the end (for instance 11, 9, 5, 3, 1, ..., 25)

## Random array

* Pour le random array nous avons utiliser le block de code suivant :
  * `ARRAY_LENGTH = 1_000`
  * `random_array = [randint(0, 1000) for i in range(ARRAY_LENGTH)]`
* Les résultats obtenus sont les suivant:

| Algorithme     | Minimum | Maximum | Average |
|----------------|---------|---------|---------|
| bubble_sort    |   0.07223469999917143      |    0.0723632009994617    |    0.07229895049931656    |
| insertion_sort |    0.03130499999861058     |   0.03184549999969022      |    0.0315752499991504     |
| merge_sort     |    0.0030542999993485864     |    0.004741799999465002     |    0.003898049999406794     |
| quicksort      |     0.0015041999995446531    |    0.0016273000001092441     |     0.0015657499998269486    |

Comme nous pouvons le constaté l'algorithme quicksort est plus performant sur le random array que les autres algo.
Cela est du au fait que quicksort :

* Possede une complexité quadratique moyenne, superieure que les autres algo comme bubbl_sort ...
* Divise généralement le tableau en partitions relativement équilibrées, ce qui minimise le nombre de comparaisons et d'échanges nécessaires.

## Sorted array

* Pour le sorted array nous avons utiliser le block de code suivant :
  * `ARRAY_LENGTH = 1_000`
  * `sorted_array = [i for i in range(ARRAY_LENGTH)]`
* Les résultats obtenus sont les suivant:

| Algorithme     | Minimum | Maximum | Average |
|----------------|---------|---------|---------|
| bubble_sort    |   8.100000013655517e-05      |    8.629999865661375e-05    |    8.364999939658446e-05    |
| insertion_sort |    0.0001472000003559515     |   0.0001506000007793773      |    0.0001489000005676644     |
| merge_sort     |    0.0019518999997671926     |    0.0020660999998654006     |    0.0020089999998162966     |
| quicksort      |     0.0017078999990189914    |    0.0018727000006037997     |     0.0017902999998113955    |

Comme nous pouvons le constaté l'algorithme bubble_sort est plus performant sur le sorted array que les autres algo.
Cela est du au fait que bubble_sort :

* Est bien sur un petit tableau d'autant plus déja trier.
* Attention sont temps d'exécution augmente rapidement avec la taille du tableau.

## sorted_min_at_end array

* Pour le sorted array nous avons utiliser le block de code suivant :
  * `ARRAY_LENGTH = 1_000`
  * `sorted_min_at_end = sorted_array[::]`
* Les résultats obtenus sont les suivant:

| Algorithme     | Minimum | Maximum | Average |
|----------------|---------|---------|---------|
| bubble_sort    |   0.036990599999626284      |    0.0395699999990029    |    0.03828029999931459    |
| insertion_sort |    0.00028190000011818483     |   0.0002994999995280523      |    0.00029069999982311856     |
| merge_sort     |    0.002187400001275819     |    0.003201699999408447     |    0.002694550000342133     |
| quicksort      |     0.0017303000004176283    |    0.0022724999998899875     |     0.002001400000153808    |

Comme nous pouvons le constaté l'algorithme insertion_sort est plus performant sur le sorted_min_at_end array que les autres algo.

## reversed array

* Pour le reversed array nous avons utiliser le block de code suivant :
  * `ARRAY_LENGTH = 1_000`
  * `sorted_min_at_end = sorted_array[::]`
    `sorted_min_at_end.append(sorted_min_at_end.pop(0))`
* Les résultats obtenus sont les suivant:

| Algorithme     | Minimum | Maximum | Average |
|----------------|---------|---------|---------|
| bubble_sort    |   0.09344020000025921      |    0.09555999999975029    |    0.09450010000000475    |
| insertion_sort |    0.06596160000117379     |   0.07325950100130285      |    0.06961055050123832     |
| merge_sort     |    0.002218199999333592     |    0.0022772000011173077     |    0.00224770000022545     |
| quicksort      |     0.001889199998913682    |    0.0018916000008175615     |     0.0018903999998656218    |

Comme nous pouvons le constaté l'algorithme quicksort est plus performant sur le reversed array que les autres algo.
Cela est du au fait que quicksort :

* Effectue un nombre réduit de comparaisons et d'échanges, ce qui améliore sa performance.

## reversed_array_max_end array

* Pour le reversed_array_max_end array nous avons utiliser le block de code suivant :
  * `ARRAY_LENGTH = 1_000`
  * `reversed_array_max_end = reversed_array[::]`
    `reversed_array_max_end.append(reversed_array_max_end.pop(0))`
* Les résultats obtenus sont les suivant:

| Algorithme     | Minimum | Maximum | Average |
|----------------|---------|---------|---------|
| bubble_sort    |   0.09029089999967255      |    0.09993959999883373    |    0.09511524999925314    |
| insertion_sort |    0.06072820000008505     |   0.06400650000068708      |    0.06236735000038607     |
| merge_sort     |    0.002304899999217014     |    0.0025165999995806487     |    0.0024107499993988313     |
| quicksort      |     0.001701699999102857    |    0.001706900000499445     |     0.001704299999801151    |

Comme nous pouvons le constaté l'algorithme quicksort est plus performant sur le reversed_array_max_end array que les autres algo.
Cela est du au fait que quicksort :

* idem que dans les autres cas ou quicksort est plus rapide.

## Sort python

* Question bonus :
  
* la methode python est plus rapide car nous utilisons directement le module fournie par python et il n'a pas besoin d'excecuter notre code et perdre du temps à l'executer
