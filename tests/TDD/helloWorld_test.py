import pytest
from helloworld.helloWorld import helloWorld


def test_return_string():
    res = helloWorld()
    assert type(res) == str


def test_return_Hello_World():
    res = helloWorld()
    assert res == "Hello World"
