import pytest
from calculator.calculator import *

@pytest.mark.parametrize("a,b,expected",[(1,2,3),("test","test","testtest"),(2.2,2.2,4.4)(complex(1,1),complex(1,2),complex(2,3))])
def test_addition(a,b,expected):
        res = addition(a,b)
        assert res == expected

@pytest.mark.parametrize("a,b,expected,exception",[(2,1,1,None),("test","test","",TypeError),(complex(2,3),complex(1,2),complex(1,1),None)])
def test_soustraction(a,b,expected,exception):
        if exception is not None:
                with pytest.raises(TypeError):
                       substraction(a,b)
        else:
            res = substraction(a,b)
            assert res == expected

@pytest.mark.parametrize("a,b,expected",[(2,2,4)])
def test_multiplication(a,b,expected):
        res = multiplication(a,b)
        assert res == expected

@pytest.mark.parametrize("a,b,expected,exception",[(1,0,0,ZeroDivisionError),(6,3,3,None),(1.2,1.2,1,None)])
def test_division(a,b,expected,exception):
        if exception is not None:
                with pytest.raises(ZeroDivisionError):
                       division(a,b)
        else:
            res = division(a,b)
            assert res == expected