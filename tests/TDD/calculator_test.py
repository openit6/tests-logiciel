import pytest
from calculator.calculator import *


@pytest.mark.parametrize(
    "a,b,expected,exception",
    [
        (1, 2, 3, None),
        (1, 5, 6, None),
        (1, "6", 7, TypeError),
        (5, -5, 0, None),
        ("Hel", "lo", "Hello", None),
    ],
)
def test_addition(a, b, expected, exception):
    if exception is not None:
        with pytest.raises(TypeError):
            addition(a, b)
    else:
        res = addition(a, b)
        assert res == expected


@pytest.mark.parametrize(
    "a,b,expected,exception",
    [
        (3, 2, 1, None),
        (6, 6, 0, None),
        ("6", 5, None, TypeError),
        (5, -5, 10, None),
        ("Hello", "lo", None, TypeError),
    ],
)
def test_substraction(a, b, expected, exception):
    if exception is not None:
        with pytest.raises(TypeError):
            substraction(a, b)
    else:
        res = substraction(a, b)
        assert res == expected


@pytest.mark.parametrize(
    "a,b,expected,exception",
    [
        (4, 2, 2, None),
        (6, 6, 1, None),
        (10, 0, None, ZeroDivisionError),
        (3, 2, 1.5, None),
    ],
)
def test_division(a, b, expected, exception):
    if exception is not None:
        with pytest.raises(exception):
            division(a, b)
    else:
        res = division(a, b)
        assert res == expected


@pytest.mark.parametrize(
    "a,b,expected,exception",
    [
        (3, 2, 6, None),
        (6, 6, 36, None),
        ("6", 5, "66666", None),
        (5, -5, -25, None),
        ("Hello", "lo", None, TypeError),
    ],
)
def test_multiplication(a, b, expected, exception):
    if exception is not None:
        with pytest.raises(exception):
            multiplication(a, b)
    else:
        res = multiplication(a, b)
        assert res == expected
