from stringlib.stringlib import basic_capitalize
import pytest
from stringlib.stringlib import safe_capitalize


def test_capitalize():
    # initialize
    value = "test value"
    expected = "Test value"
    # act
    res = basic_capitalize(value)
    # test
    assert res == expected


# TODO Write a test for empty values
def test_empty_capitalize():
    value = ''
    sortie = 'fin'
    res = basic_capitalize(value)
    assert value == res


# TODO Write a test for string starting with a number
def test_number_capitalize():
    value = "1hello"
    value2 = "1hello"
    res = basic_capitalize(value)
    assert value == res


# TODO Write a test to find a normal failure
# AttributeError exception is sent when data type is not correct
# To test an exception is sent use
#
# with pytest.raise(ExceptionClass):
#   your code to test
#
def test_exception_capitalize():
    with pytest.raises(AttributeError):
        value = None
        basic_capitalize(value)

# TODO Write a test for none values (value = None)
def test_none_capitalize():
    value = None
    res = basic_capitalize(value)
    pass


# TODO Write a new safer version of capitalize which return the input value
# if the type is not string
# To test if an object is an instance of a given class use isinstance(data, class):
# The name of the new function will be safe_capitalize
def test_wrong_type_capitalize():
    val = "test"
    val2 = "test"
    res = safe_capitalize(val)
    assert res == val2
    error = 2
    resEroor = safe_capitalize(error)
    assert error == resEroor


# TODO Add parametrized tests
# with the following input values "normal string", "", "123 string"
# see https://docs.pytest.org/en/stable/parametrize.html
@pytest.mark.parametrize(
    "val,expect,error",
    [("test", "Test", None), (6, "Test", AttributeError)],
)
def test_capitalize_values(val, expect, error):
    if error is not None:
        with pytest.raises(AttributeError):
            basic_capitalize(val)
    else:
        res = basic_capitalize(val)
        assert res == expect