from stringlib.stringlib import basic_capitalize, safe_capitalize
import pytest


def test_capitalize():
    # initialize
    value = "test value"
    expected = "Test value"
    # act
    res = basic_capitalize(value)
    # test
    assert res == expected


# DONE Write a test for empty values
def test_empty_capitalize():
    value = ""
    expected = ""
    res = basic_capitalize(value)
    assert res == expected
    # pass


# DONE Write a test for string starting with a number
def test_number_capitalize():
    value = "3test"
    expected = "3test"
    res = basic_capitalize(value)

    assert res == expected
    # pass


# DONE Write a test to find a normal failure
# AttributeError exception is sent when data type is not correct
# To test an exception is sent use
#
# with pytest.raises(ExceptionClass):
#   your code to test
#
def test_exception_capitalize():
    with pytest.raises(AttributeError):
        value = 4
        basic_capitalize(value)


# DONE  Write a test for none values (value = None)
def test_none_capitalize():
    with pytest.raises(AttributeError):
        value = None
        basic_capitalize(value)
        # pass


# Done Write a new safer version of capitalize which return the input value
# if the type is not string
# To test if an object is an instance of a given class use isinstance(data, class):
# The name of the new function will be safe_capitalize
def test_wrong_type_capitalize():
    valueCap = "this is a test"
    expectedCap = "This is a test"
    resCap = safe_capitalize(valueCap)
    assert resCap == expectedCap
    valueWrong = 10
    resWrong = safe_capitalize(valueWrong)
    assert valueWrong == resWrong


# DONE  Add parametrized tests
# with the following input values "normal string", "", "123 string"
# see https://docs.pytest.org/en/stable/parametrize.html
@pytest.mark.parametrize(
    "value,expected,exception",
    [("test", "Test", None), (6, "Test", AttributeError)],
)
def test_capitalize_values(value, expected, exception):
    if exception is not None:
        with pytest.raises(exception):
            basic_capitalize(value)
    else:
        res = basic_capitalize(value)
        assert res == expected
