# Install RFW

## on WSL

### Google Chrome

```shell
sudo apt-get install -y curl unzip xvfb libxi6 libgconf-2-4
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome-stable_current_amd64.deb
google-chrome --version
```

### ChromeDriver

```shell
wget https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/117.0.5938.92/linux64/chromedriver-linux64.zip
unzip chromedriver_linux64.zip
unzip chromedriver-linux64.zip
sudo mv chromedriver /usr/bin/chromedriver
sudo mv chromedriver-linux64/chromedriver /usr/bin/chromedriver
sudo chmod +x /usr/bin/chromedriver
sudo chown root:root /usr/bin/chromedriver
chromedriver --version
```

### Connect WSL to host display

Line to add to `.bashrc` or equivalent
`export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0`

## on Windows

Download chromedriver for win64 from [this link](https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/118.0.5993.18/win64/chromedriver-win64.zip)

extract the file `chromedriver.exe` and put it in a folder accessible from your PATH

Download chromium for win64 from [this link](https://edgedl.me.gvt1.com/edgedl/chrome/chrome-for-testing/118.0.5993.18/win64/chrome-win64.zip)

extract the folder `chrome-win64` and put it anywhere you want (your documents for example), go inside and create a shortcut of the file `chrome.exe`. Move the shorcut next to your `chromedriver.exe` and rename it in `chrome.exe`

### RobotFramework & Dependancies

requirements.txt:

```requirements.txt
robotframework
robotframework-selenium2library
pyyaml
```
