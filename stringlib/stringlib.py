def basic_capitalize(data: str) -> str:
    return data.capitalize()


def safe_capitalize(data: str) -> str:
    if type(data) != str:
        return data
    return data.capitalize()
